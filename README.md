# OpenML dataset: UMIST_Faces_Cropped

https://www.openml.org/d/41084

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   H. Wechsler, P. J. Phillips, V. Bruce, F. Fogelman-Soulie and T. S. Huang
**Source**:  https://www.sheffield.ac.uk/eee/research/iel/research/face  
**Please cite**:  Characterizing Virtual Eigensignatures for General Purpose Face Recognition, Daniel B Graham and Nigel M Allinson. In Face Recognition: From Theory to Applications ; NATO ASI Series F, Computer and Systems Sciences, Vol. 163; H. Wechsler, P. J. Phillips, V. Bruce, F. Fogelman-Soulie and T. S. Huang (eds), pp 446-456, 1998. 

The Sheffield (previously UMIST) Face Database consists of 564 images of 20 individuals (mixed race/gender/appearance). Each individual is shown in a range of poses from profile to frontal views - each in a separate directory labelled 1a, 1b, ... 1t and images are numbered consecutively as they were taken

Grayscale faces 8 bit [0-255], a few images (views) of 20 different people.
575 total images, 112x92 size, manually cropped by Daniel Graham at UMist

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41084) of an [OpenML dataset](https://www.openml.org/d/41084). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41084/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41084/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41084/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

